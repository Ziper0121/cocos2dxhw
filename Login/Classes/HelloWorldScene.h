#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__
#include "ui/CocosGUI.h"
#include <cstring>
#include <map>

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

	//Login 버튼을 받는 변수
	cocos2d::ui::Button* Login_button;
	//Login 실패 창 변수
	cocos2d::ui::Button* Login_failure; 
	//Login 성공 창 변수
	cocos2d::ui::Button* Login_success;



    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);


	//ID를 받는 EditBox
	cocos2d::ui::EditBox* EditBox_ID;
	cocos2d::ui::EditBox* EditBox_Passworld;

	/* 함수 */

	//이벤트 터치에 값을 받아 검사한다.
	bool touch(cocos2d::Touch* Tch, cocos2d::Event* Et);
};

#endif // __HELLOWORLD_SCENE_H__
