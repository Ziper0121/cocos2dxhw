#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "ui/CocosGUI.h"
#include "cocos2d.h"
#include <vector>

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
	//TouchOneByOne
	void TouchOneByOne();
	bool testTouchBegan(cocos2d::Touch* Tch, cocos2d::Event* Et);
	void testTouchMoved(cocos2d::Touch* Tch, cocos2d::Event* Et);
	void testTouchEnded(cocos2d::Touch* Tch, cocos2d::Event* Et);
	void testTouchCancelled(cocos2d::Touch* Tch, cocos2d::Event* Et);

	//EventListenerTouchAllAtOnce const std::vector<Touch*>&
	void EventListenerTouchAllAtOnce();
	void testTouchesBegan(const std::vector<cocos2d::Touch*>& Tch, cocos2d::Event* Et);
	void testTouchesMoved(const std::vector<cocos2d::Touch*>& Tch, cocos2d::Event* Et);
	void testTouchesEnded(const std::vector<cocos2d::Touch*>& Tch, cocos2d::Event* Et);
	void testTouchesCancelled(const std::vector<cocos2d::Touch*>& Tch, cocos2d::Event* Et);

	//Keyboard
	void Event_Keyboard();
	void testKeyboardpress(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et);
	void testKeyboardRelease(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et);

	//UI touch / clock handling
	void UI_Touch_click();
	void testUITouch(cocos2d::Ref* rf, cocos2d::ui::Widget::TouchEventType et);
	void restUIClick(cocos2d::Ref* rf);
};

#endif // __HELLOWORLD_SCENE_H__
