#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	//HelloWorld::TouchOneByOne();
	//HelloWorld::EventListenerTouchAllAtOnce();
	//HelloWorld::Event_Keyboard();
	//HelloWorld::UI_Touch_click();
	return true;
}
//TouchOneByOne  하나의 터치를 받는 넘
void HelloWorld::TouchOneByOne(){
	auto touch_one = cocos2d::EventListenerTouchOneByOne::create();
	touch_one->onTouchBegan = std::bind(&HelloWorld::testTouchBegan, 
		this, std::placeholders::_1, std::placeholders::_2);
	touch_one->onTouchMoved = std::bind(&HelloWorld::testTouchMoved,
		this, std::placeholders::_1, std::placeholders::_2);
	touch_one->onTouchEnded = std::bind(&HelloWorld::testTouchEnded,
		this, std::placeholders::_1, std::placeholders::_2);
	touch_one->onTouchCancelled = std::bind(&HelloWorld::testTouchCancelled,
		this, std::placeholders::_1, std::placeholders::_2);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touch_one, this);
}
//시작할때 터치가 되는 곳을 인스턴트로 넘기는 넘
bool HelloWorld::testTouchBegan(cocos2d::Touch* Tch, cocos2d::Event* Et){
	CCLOG("touch began point : (%.2f,%.2f)", Tch->getLocation().x, Tch->getLocation().y);
	return true;
}
//터치가 움직이는 곳을 인스턴트로 넘기는 넘
void HelloWorld::testTouchMoved(cocos2d::Touch* Tch, cocos2d::Event* Et){
	CCLOG("touch moved point : (%.2f,%.2f)", Tch->getLocation().x, Tch->getLocation().y);
}
//끝날때 터치가 되는 곳을 인스턴트를 넘기는 넘
void HelloWorld::testTouchEnded(cocos2d::Touch* Tch, cocos2d::Event* Et){
	CCLOG("touch ended point : (%.2f,%.2f)", Tch->getLocation().x, Tch->getLocation().y);
}
//부득이에 터치가 안되는 경우 터치의 위치를 인스턴트로 넘기는 넘
void HelloWorld::testTouchCancelled(cocos2d::Touch* Tch, cocos2d::Event* Et){
	CCLOG("touch cancelled point : (%.2f,%.2f)", Tch->getLocation().x, Tch->getLocation().y);
}

//EventListenerTouchAllAtOnce const std::vector<Touch*>& 
void HelloWorld::EventListenerTouchAllAtOnce(){
	auto touch_one = cocos2d::EventListenerTouchAllAtOnce::create();
	touch_one->onTouchesBegan = std::bind(&HelloWorld::testTouchesBegan,
		this, std::placeholders::_1, std::placeholders::_2);
	touch_one->onTouchesMoved = std::bind(&HelloWorld::testTouchesMoved,
		this, std::placeholders::_1, std::placeholders::_2);
	touch_one->onTouchesEnded = std::bind(&HelloWorld::testTouchesEnded,
		this, std::placeholders::_1, std::placeholders::_2);
	touch_one->onTouchesCancelled = std::bind(&HelloWorld::testTouchesCancelled,
		this, std::placeholders::_1, std::placeholders::_2);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touch_one, this);
}
void HelloWorld::testTouchesBegan(const std::vector<cocos2d::Touch*>& Tch, cocos2d::Event* Et){
	CCLOG("multi-touch handling (began)");
	for (std::vector<cocos2d::Touch*>::const_iterator it = Tch.begin(); it < Tch.end(); it++){
		cocos2d::Vec2 loc = (*it)->getLocation();
		CCLOG("point(%.2f,%.2f)", loc.x, loc.y);
	}
}
void HelloWorld::testTouchesMoved(const std::vector<cocos2d::Touch*>& Tch, cocos2d::Event* Et){
	CCLOG("multi-touch handling (moverd)");
	for (std::vector<cocos2d::Touch*>::const_iterator it = Tch.begin(); it < Tch.end(); it++){
		cocos2d::Vec2 loc = (*it)->getLocation();
		CCLOG("point(%.2f,%.2f)", loc.x, loc.y);
	}
}
void HelloWorld::testTouchesEnded(const std::vector<cocos2d::Touch*>& Tch, cocos2d::Event* Et){
	CCLOG("multi-touch handling (ended)");
	for (std::vector<cocos2d::Touch*>::const_iterator it = Tch.begin(); it < Tch.end(); it++){
		cocos2d::Vec2 loc = (*it)->getLocation();
		CCLOG("point(%.2f,%.2f)", loc.x, loc.y);
	}
}
void HelloWorld::testTouchesCancelled(const std::vector<cocos2d::Touch*>& Tch, cocos2d::Event* Et){
	CCLOG("multi-touch handling (cancelled)");
	for (std::vector<cocos2d::Touch*>::const_iterator it = Tch.begin(); it < Tch.end(); it++){
		cocos2d::Vec2 loc = (*it)->getLocation();
		CCLOG("point(%.2f,%.2f)", loc.x, loc.y);
	}
}

//Keyboards
void HelloWorld::Event_Keyboard(){
	auto keybd = cocos2d::EventListenerKeyboard::create();
	keybd->onKeyPressed = std::bind(&HelloWorld::testKeyboardpress, this, std::placeholders::_1, std::placeholders::_2);
	keybd->onKeyReleased = std::bind(&HelloWorld::testKeyboardRelease, this, std::placeholders::_1, std::placeholders::_2);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keybd, this);
}
void HelloWorld::testKeyboardpress(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et){
	CCLOG("press Keyboard : (CODE %d)", Key);
}
void HelloWorld::testKeyboardRelease(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et){
	CCLOG("Release Keyboard : (CODE %d)", Key);
}

//UI touch / clock handling
void HelloWorld::UI_Touch_click(){
	cocos2d::ui::Button* button = cocos2d::ui::Button::create("mario.png");
	// == 터치 이벤트 리스너 등록
	button->addTouchEventListener(std::bind(&HelloWorld::testUITouch, this, std::placeholders::_1, std::placeholders::_2));
	// == 클릭 이벤트 리스너 등록
	button->addClickEventListener(std::bind(&HelloWorld::restUIClick, this, std::placeholders::_1));
	button->setPosition(cocos2d::Vec2(500, 200));
	addChild(button);
}
void HelloWorld::testUITouch(cocos2d::Ref* rf, cocos2d::ui::Widget::TouchEventType et){
	cocos2d::ui::Widget* t = (cocos2d::ui::Widget*)rf;
	switch (et){
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		CCLOG("ui touch event Began (%.2f,%.2f)", t->getTouchBeganPosition().x, t->getTouchBeganPosition().y);
		break;
		//무브의 값이 변하지 않는다.
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		CCLOG("ui touch event MOVED (%.2f,%.2f)", t->getTouchBeganPosition().x, t->getTouchBeganPosition().y);
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		CCLOG("ui touch event ENDED (%.2f,%.2f)", t->getTouchBeganPosition().x, t->getTouchBeganPosition().y);
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		CCLOG("ui touch event Canceled ");
		break;
	}
}
void HelloWorld::restUIClick(cocos2d::Ref* rf){
	CCLOG("ui click");
}