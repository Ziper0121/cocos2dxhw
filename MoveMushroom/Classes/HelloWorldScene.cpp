#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

	HelloWorld::mushroom = cocos2d::Sprite::create("stand.png");
	mushroom->setPosition(cocos2d::Vec2(300, 300));
	this->addChild(mushroom);

	auto Tuch_KeyBoard = cocos2d::EventListenerKeyboard::create();
	Tuch_KeyBoard->onKeyPressed = std::bind(&HelloWorld::bool_Keyboard_down, this, std::placeholders::_1, std::placeholders::_2);
	Tuch_KeyBoard->onKeyReleased = std::bind(&HelloWorld::bool_Keyboard_up, this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(Tuch_KeyBoard, this);

	this->schedule(schedule_selector(HelloWorld::logic));
	this->schedule(schedule_selector(HelloWorld::jumplogic));
	this->schedule(schedule_selector(HelloWorld::stopLogic));
    return true;
}
void HelloWorld::bool_Keyboard_down(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et){
	Animation* Run_Mushroom = Animation::create();
	//오른쪽
	if (Key == cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
		HelloWorld::bool_Keyboard_Right = true;
		HelloWorld::mushroom->setFlippedX(true);
	}
	//왼쪽
	if (Key == cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		HelloWorld::bool_Keyboard_Left = true;
		HelloWorld::mushroom->setFlippedX(false);
	}
	if (Key == EventKeyboard::KeyCode::KEY_SPACE && force <= 0){
		mushroom->stopActionByTag(stopRun_int);
		mushroom->stopActionByTag(2);
		force = 30.0f;
		if (bool_Keyboard_Right || bool_Keyboard_Left && force == 0){
			Animation* Jump_And_Run = Animation::create();
			Jump_And_Run->addSpriteFrameWithFile("run1.png");
			Jump_And_Run->addSpriteFrameWithFile("run2.png");
			Jump_And_Run->addSpriteFrameWithFile("run3.png");
			Jump_And_Run->addSpriteFrameWithFile("run2.png");
			Jump_And_Run->setDelayPerUnit(0.3f);
			HelloWorld::mushroom->runAction(RepeatForever::create(Animate::create(Jump_And_Run)))->setTag(2);
		}
	}
	if (Key == cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW
		|| Key == cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW
		&& Speed == 0){
		mushroom->stopActionByTag(2);
		Run_Mushroom->addSpriteFrameWithFile("run1.png");
		Run_Mushroom->addSpriteFrameWithFile("run2.png");
		Run_Mushroom->addSpriteFrameWithFile("run3.png");
		Run_Mushroom->addSpriteFrameWithFile("run2.png");
		Run_Mushroom->setDelayPerUnit(0.3f);
		Speed = 2.0f;
	}
	HelloWorld::mushroom->runAction(RepeatForever::create(Animate::create(Run_Mushroom)))->setTag(stopRun_int);
}
void HelloWorld::bool_Keyboard_up(cocos2d::EventKeyboard::KeyCode Key, cocos2d::Event* Et){
	//오른쪽
	if (Key == EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
		bool_Keyboard_Right = false;
	}
	//왼쪽
	if (Key == EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		bool_Keyboard_Left = false;
	}
}
void HelloWorld::logic(float dt){
	cocos2d::Vec2 pos = HelloWorld::mushroom->getPosition();
	if (HelloWorld::bool_Keyboard_Left){
		pos.x -= Speed;
	}
	if (HelloWorld::bool_Keyboard_Right){
		pos.x += Speed;
	}
	HelloWorld::mushroom->setPosition(pos);
}

void HelloWorld::jumplogic(float dt){
	Vec2 pos = mushroom->getPosition();
	force = force - (dt)* 200;
	pos.y = pos.y + force;
	if (pos.y >= 300.0f){
		mushroom->setTexture("jump.png");
		mushroom->setPosition(pos);
	}
	else if (pos.y < 300.0f && mushroom->getPosition().y != 300.0f){
		mushroom->setPosition(pos.x, 300.0f);
	}
}
void HelloWorld::stopLogic(float dt){
	//완전한 정지
	if (!bool_Keyboard_Left
		&&!bool_Keyboard_Right
		&& force <= 0){
		mushroom->stopAllActions();
		mushroom->setTexture("stand.png");
		Speed = 0.0f;
	}
}