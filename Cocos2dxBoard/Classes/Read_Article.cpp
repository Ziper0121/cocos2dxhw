#include "Read_Article.h"

using namespace cocos2d;

cocos2d::Scene* Read_Article::Read_Article_scene(){
	cocos2d::Scene* Read_Article_of = cocos2d::Scene::create();
	Read_Article * P_Read_Article = Read_Article::create();
	Read_Article_of->addChild(P_Read_Article);
	return Read_Article_of;
}

bool Read_Article::init(){
	if (!cocos2d::Layer::init()) return false;
	Read_Article::List_Back_Button = cocos2d::ui::Button::create("background_textbox.png");
	Read_Article::List_Back_Button->setPosition(cocos2d::Vec2(100, 100));
	Read_Article::List_Back_Button->setSwallowTouches(false);
	Read_Article::List_Back_Button->setTitleColor(cocos2d::Color3B(1, 1, 0));
	Read_Article::List_Back_Button->setTitleText("List");
	this->addChild(Read_Article::List_Back_Button);

	Read_Article::Article_name_scrin = cocos2d::Label::create(Article_List::Article_name[Article_List::number].c_str(), "04B_21__.ttf", 24.0f);
	Read_Article::Article_name_scrin->setPosition(Vec2(200,200));
	addChild(Read_Article::Article_name_scrin);
	Read_Article::Article_in_scrin = cocos2d::Label::create(Article_List::Article_in[Article_List::number].c_str(), "04B_21__.ttf", 24.0f);
	Read_Article::Article_in_scrin->setPosition(Vec2(300, 300));
	addChild(Read_Article::Article_in_scrin);

	auto Lamda_Mous_Touch = cocos2d::EventListenerTouchOneByOne::create();
	Lamda_Mous_Touch->onTouchBegan = std::bind(&Read_Article::Touch_maus, this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(Lamda_Mous_Touch, this);

	return true;
}
bool Read_Article::Touch_maus(cocos2d::Touch* Tch, cocos2d::Event* Et){
	cocos2d::Rect List_Rect = Read_Article::List_Back_Button->getBoundingBox();
	if (List_Rect.containsPoint(Tch->getLocation())){
		cocos2d::Director::getInstance()->replaceScene(Article_List::Article_List_Scene());
	}
	return true;
}