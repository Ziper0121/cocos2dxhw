#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__
//cocos2d의 기본 해더
#include "cocos2d.h"
#include "ui/CocosGUI.h"
//c++꺼
#include <map>
#include <string>
//내가 만든 것
#include "Article_List.h"
class HelloWorld : public cocos2d::Layer
{
public:
	/*기본적인 것*/
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
	/*내가 만드는 것*/
	/*          변수          */
	//유저한테 아이디를 받는 
	std::map<std::string, std::string> User_Get_Id_Password_map;
	std::map<std::string, std::string>::iterator Get_Id_Pass_map_compare_Edit;
	//ID입력 창
	cocos2d::ui::EditBox* User_Get_Id_Write = nullptr;
	//password 입력 창
	cocos2d::ui::EditBox* User_Get_Password_Write = nullptr;
	
	//Login 버튼
	cocos2d::ui::Button* Login_Button = nullptr;
	/*          함수          */
	//touch하여 Article List로 이동 하는 것
	bool HelloWorld_go_Article_Liset_touch(cocos2d::Touch* Tch, cocos2d::Event* Et);
};

#endif // __HELLOWORLD_SCENE_H__
