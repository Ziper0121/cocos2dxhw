#ifndef __Read_Article__h_
#define __Read_Article__h_
#include "Article_List.h"

class Read_Article : public cocos2d::Layer{
public:
	//Read_Article의 씬 
	static cocos2d::Scene* Read_Article_scene();
	CREATE_FUNC(Read_Article);

	//몸체 ?
	virtual bool init();

	/*          변수          */
	//돌아가는 버튼
	cocos2d::ui::Button* List_Back_Button;

	cocos2d::Label* Article_name_scrin;
	cocos2d::Label* Article_in_scrin;

	//버튼 터치
	bool Touch_maus(cocos2d::Touch* Tch, cocos2d::Event* Et);

};

#endif //__Read_Article__h_