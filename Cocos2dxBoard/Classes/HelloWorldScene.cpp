#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	//유저의 아이디와 페스워드를 받는 맵
	HelloWorld::User_Get_Id_Password_map
		.insert(std::pair<std::string, std::string>("KJH", "1234"));
	//유저의 받은 아이디와 패스워드를 비교하는 맵
	HelloWorld::Get_Id_Pass_map_compare_Edit 
		= HelloWorld::User_Get_Id_Password_map.begin();
	
	//유저가 아이디를 쓰는 곳
	HelloWorld::User_Get_Id_Write = cocos2d::ui::EditBox::create
		(cocos2d::Size(300, 50), "background_textbox.png");
	HelloWorld::User_Get_Id_Write->setFontSize(30.0f);
	HelloWorld::User_Get_Id_Write->setFontColor(cocos2d::Color3B(1, 1, 0));
	HelloWorld::User_Get_Id_Write->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f / 2.0f ));
	this->addChild(HelloWorld::User_Get_Id_Write);
	//유저의 비밀번호를 쓰는 곳
	HelloWorld::User_Get_Password_Write = cocos2d::ui::EditBox::create
		(cocos2d::Size(300, 50), "background_textbox.png");
	HelloWorld::User_Get_Password_Write->setFontSize(30.0f);
	HelloWorld::User_Get_Password_Write->setFontColor(cocos2d::Color3B(1, 1, 0));
	HelloWorld::User_Get_Password_Write->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f / 2.0f - 30.0f));
	this->addChild(HelloWorld::User_Get_Password_Write);
    
	//로그인 버튼
	HelloWorld::Login_Button = cocos2d::ui::Button::create
		("btn_login_up.png", "btn_login_down.png");
	HelloWorld::Login_Button->setSwallowTouches(false);
	HelloWorld::Login_Button->setPosition(cocos2d::Vec2(960.0f / 2.0f, 640.0f / 2 - 90));
	this->addChild(HelloWorld::Login_Button);

	//login을 실행시키는 람다
	auto Lamda_Maus_Touch = cocos2d::EventListenerTouchOneByOne::create();
	Lamda_Maus_Touch->onTouchBegan = std::bind(&HelloWorld::HelloWorld_go_Article_Liset_touch,
		this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(Lamda_Maus_Touch, this);

	return true;
}
bool HelloWorld::HelloWorld_go_Article_Liset_touch
(cocos2d::Touch* Tch, cocos2d::Event* Et)
{
	Rect Button_Login = Login_Button->getBoundingBox();
	if (Button_Login.containsPoint(Tch->getLocation())){
		char* get_Write_ID;
		char* get_Write_PassWord;
		get_Write_ID = (char*)HelloWorld::User_Get_Id_Write->getText();
		get_Write_PassWord = (char*)HelloWorld::User_Get_Password_Write->getText();

		if (strcmp(get_Write_ID, HelloWorld::Get_Id_Pass_map_compare_Edit->first.c_str()) == 0
			&& strcmp(get_Write_PassWord, HelloWorld::Get_Id_Pass_map_compare_Edit->second.c_str()) == 0){
			Director::getInstance()->replaceScene(Article_List::Article_List_Scene());
		}
	}
	return true;
}