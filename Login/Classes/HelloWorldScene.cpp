#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;
using namespace cocos2d;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }


	HelloWorld::Login_failure = cocos2d::ui::Button::create("btn_up.png", "btn_down.png", "btn_disabled.png");
	Login_failure->setTitleText("failure");
	Login_failure->setTitleColor(Color3B(1, 0, 0));
	Login_failure->setTitleFontSize(24.0f);
	Login_failure->setSwallowTouches(false);
	Login_failure->setEnabled(false);
	Login_failure->setPosition(Vec2(960.0f / 2.0f, 640.0f / 2.0f - 100.0f));
	this->addChild(Login_failure);

	HelloWorld::Login_success = cocos2d::ui::Button::create("btn_up.png", "btn_down.png", "btn_disabled.png");
	Login_success->setTitleText("failure");
	Login_success->setTitleColor(Color3B(1, 0, 0));
	Login_success->setTitleFontSize(24.0f);
	Login_success->setSwallowTouches(false);
	Login_success->setEnabled(false);
	Login_success->setPosition(Vec2(960.0f / 2.0f, 640.0f / 2.0f));
	this->addChild(Login_success);

	HelloWorld::Login_button = cocos2d::ui::Button::create("btn_up.png","btn_down.png","btn_disabled.png");
	Login_button->setTitleText("Login");
	Login_button->setTitleColor(Color3B(1, 0, 0));
	Login_button->setTitleFontSize(24.0f);
	Login_button->setSwallowTouches(false);
	Login_button->setPosition(Vec2(960.0f / 2.0f, 640.0f / 2.0f - 110.0f));
	this->addChild(Login_button);


	HelloWorld::EditBox_ID = cocos2d::ui::EditBox::create(Size(300.0f, 30.0f), "editbox_background.png");
	EditBox_ID->setFontSize(20.0f);
	EditBox_ID->setPosition(Vec2(960 / 2, 640 / 2));
	this->addChild(EditBox_ID);

	HelloWorld::EditBox_Passworld = cocos2d::ui::EditBox::create(Size(300.0f, 30.0f), "editbox_background.png");
	EditBox_Passworld->setFontSize(20.0f);
	EditBox_Passworld->setPosition(Vec2(960.0f / 2.0f, 640.0f / 2.0f - 60.0f));
	this->addChild(EditBox_Passworld);


	auto Touch_event = cocos2d::EventListenerTouchOneByOne::create();
	Touch_event->onTouchBegan = std::bind(&HelloWorld::touch, this, std::placeholders::_1, std::placeholders::_2);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(Touch_event, this);

    return true;
}
bool HelloWorld::touch(cocos2d::Touch* Tch, cocos2d::Event* Et){
	Rect rect_login = Login_button->getBoundingBox();
	Rect rect_success = Login_success->getBoundingBox();
	Rect rect_failure = Login_failure->getBoundingBox();
	if (rect_login.containsPoint(Tch->getLocation())){
		//EditBox를 char*로 받는 것이 힘들다
		if (1){
			Login_success->setEnabled(true);
			if (rect_success.containsPoint(Tch->getLocation())) Login_success->setEnabled(false);
		}
		else
		{
			Login_failure->setEnabled(true);
			if (rect_failure.containsPoint(Tch->getLocation())) Login_failure->setEnabled(false);
		}
	}

	return true;
}